from django import forms
from paranoia.models import UserProfile
from registration.models import User


class UserForm(forms.ModelForm):
  first_name = forms.CharField(max_length=150)
  last_name = forms.CharField(max_length=150)

  class Meta:
    model = User
    fields = ('username', 'email', 'first_name', 'last_name')


class UserProfileForm(forms.ModelForm):
  id = forms.CharField(widget=forms.HiddenInput())
  imei = forms.CharField(max_length=200)
  nick_name = forms.CharField(max_length=100)
  loyalty = forms.IntegerField()
  is_offside = forms.BooleanField(required=False,initial=False)
  is_assistant = forms.BooleanField(required=False,initial=False)

  class Meta:
    model = UserProfile
    exclude = ['user']





