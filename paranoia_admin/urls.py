from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from paranoia_admin import views


urlpatterns = patterns('',

    url(r'^players-list', views.players_list, name='players_list'),
    url(r'^players-details', views.players_details, name='players_details'),

)