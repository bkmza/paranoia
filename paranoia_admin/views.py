from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from paranoia.models import UserProfile
from paranoia_admin.forms import UserProfileForm, UserForm

@login_required
@require_http_methods(["GET"])
def map(request):
  context = {}
  return render(request, 'admin/map.html', context)


@login_required
@require_http_methods(["GET"])
def players_list(request):
    profiles = UserProfile.objects.all()

    context = {'title': 'Players', 'profiles': profiles}
    return render(request, 'admin/players_list.html', context)

@login_required
def players_details(request):
    if request.method == 'POST':

        user_data, profile_data = getProfileData(request.POST)
        profile = UserProfile.objects.get(id=profile_data['id'])

        pForm = UserProfileForm(profile_data, instance=profile)
        uForm = UserForm(user_data, instance=profile.user)


        if uForm.is_valid():
            upd_user = uForm.save(commit=True)

            if pForm.is_valid():
                upd_profile = pForm.save(commit=False)
                upd_profile.author = upd_user
                upd_profile.save()
        else:
          return render(request, 'admin/players_details.html', {'formset': [uForm, pForm], "profile": profile})

        return HttpResponseRedirect('players-list')

    else:
        profile = UserProfile.objects.get(id=request.GET.get('pid'))
        pForm = UserProfileForm(instance=profile)
        uForm = UserForm(instance=profile.user)
        return render(request, 'admin/players_details.html', {'formset': [uForm, pForm], "profile": profile})


def getProfileData(post_data):
    raw = post_data.copy()
    user_data = {
        'username': raw['username'],
        'email': raw['email'],
        'first_name': raw['first_name'],
        'last_name': raw['last_name']
    }
    profile_data = {
        'id': raw['id'],
        'imei': raw['imei'],
        'nick_name': raw['nick_name'],
        'loyalty': raw['loyalty'],
        'is_offside': raw['is_offside'],
        'is_assistant': raw['is_assistant'],
    }
    return user_data, profile_data


