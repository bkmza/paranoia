from models import UserProfile
from forms import *

def user_created(sender, user, request, **kwargs):
    form = RegistrationExtendedForm(request.POST)
    data = UserProfile(user=user)
    data.imei = form.data["imei"]
    data.save()

from registration.signals import user_registered
user_registered.connect(user_created)