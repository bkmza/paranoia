from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from paranoia import views


urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^profile$', views.profile, name='profile'),

    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    (r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/profile/$', RedirectView.as_view(url='/')),
    url(r'^users/', RedirectView.as_view(url='/'))
)