from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from paranoia.models import UserProfile


def index(request):
    context = {'title': 'Map'}
    return render(request, 'paranoia/index.html', context)


@login_required
@require_http_methods(["GET"])
def profile(request):
    context = {'title': 'Map'}

    if request.user.is_superuser:
      context['is_superuser'] = True

    return render(request, 'paranoia/profile.html', context)


