from django.db import models
from registration.models import User
from registration.signals import user_registered


class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    imei = models.CharField(max_length=20)
    nick_name = models.CharField(max_length=200)
    loyalty = models.IntegerField(default=0)
    is_offside = models.BooleanField(default=False)
    is_assistant = models.BooleanField(default=False)

    def __unicode__(self):
      return '%s' % self.user

    def get_absolute_url(self):
      return ('profiles_profile_detail', (), { 'username': self.user.username })



def user_registered_callback(sender, user, request, **kwargs):

    user.first_name = request.POST["first_name"]
    user.last_name = request.POST["last_name"]
    user.is_active = True

    profile = UserProfile(user = user)
    profile.imei = request.POST["imei"]
    profile.nick_name = request.POST["nick_name"]
    profile.loyalty = int(request.POST["loyalty"])
    profile.is_offside = bool(request.POST["is_offside"])
    profile.is_assistant = bool(request.POST["is_assistant"])

    user.save()
    profile.save()

user_registered.connect(user_registered_callback)