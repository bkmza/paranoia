from forms import *
from django import forms
from registration.forms import RegistrationForm

from models import UserProfile

class RegistrationExtendedForm(RegistrationForm):

    first_name = forms.CharField(max_length=150)
    last_name = forms.CharField(max_length=150)

    imei = forms.CharField(max_length=200)
    nick_name = forms.CharField(max_length=100)
    loyalty = forms.IntegerField()

    is_offside = forms.BooleanField(required=False,initial=False)
    is_assistant = forms.BooleanField(required=False,initial=False)

    def save(self, profile_callback = None):
      UserProfile.objects.get_or_create(user = self.cleaned_data['user'],
                                        imei = self.cleaned_data['imei'],
                                        nick_name = self.cleaned_data['nick_name'],
                                        loyalty = self.cleaned_data['loyalty'],
                                        is_offside = self.cleaned_data['is_offside'],
                                        is_assistant = self.cleaned_data['is_assistant'])

      super(RegistrationExtendedForm, self).save(self, profile_callback)





