from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
from registration.backends.simple.views import RegistrationView
from paranoia.forms import RegistrationExtendedForm

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', RedirectView.as_view(url='/p')),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/accounts/login'}),

    url(r'accounts/register/$',
        RegistrationView.as_view(form_class = RegistrationExtendedForm),
        name = 'registration_register'),

    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/profile/$', RedirectView.as_view(url='/p/profile')),
    url(r'^users/', RedirectView.as_view(url='/p')),

    url(r'^p/', include('paranoia.urls', namespace="paranoia")),
    url(r'^p/admin/', include('paranoia_admin.urls', namespace="paranoia_admin")),

    url(r'^p-admin/alliances/', include('p_alliances.urls', namespace="p_alliances")),
    url(r'^p-admin/fractions/', include('p_fractions.urls', namespace="p_fractions")),
    url(r'^p-admin/points/', include('p_points.urls', namespace="p_points")),


    url(r'^admin/', include(admin.site.urls)),
)
