var AllianceList = function (settings) {
    var ex = {
          tableId: '#alliancesListTable'
        },
        doInitPage = function() {
          $.extend(ex, settings);

          // TODO
          // remove DT and use knockout
          // doInitTable();
          doBindRemoveBtn();
        },

        doInitTable = function(){
          $(ex.tableId).dataTable( {
              "aoColumns": [
                { "sTitle": "#", "sWidth": "12%" },
                { "sTitle": "Name", "sWidth": "40%" },
                { "sTitle": "Game Name", "sWidth": "30%" },
                { "sWidth": "10%"}
              ]
          } );
        },

        doBindRemoveBtn = function(){
          var token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
          $('.delete-item').on('click', function(){
            var id = $(this).closest('tr').attr('data-id');
            console.log(id);
            if(confirm('Are you sure that you want to delete this item?')){
              utils.doAjax(ex.urlDelete, {csrfmiddlewaretoken: token, id: id}, doCallbackRemove);
            }
          })
        },
        doCallbackRemove = function(data){
          if(data && data.is_success){
            location.reload();
          }
        };
    return {
        initPage: doInitPage
    }
}