var utils = new function(){

  this.doAjax = function(url, data, callback){
    $.ajax({
      type: 'POST',
      url: url,
      dataType : "json",
      data: data,
      success: function (data) {
        callback(data);
      }
    });
  },

  this.doAjaxForm = function(url, data, callback){
    $.ajax({
      type: 'GET',
      url: url,
      data: data,
      success: function (data) {
        callback(data);
      }
    });
  },

  this.initSelect2 = function(cls, placeholder){
    $(cls).select2({
      placeholder: placeholder
    });
  },

  this.initDatepickers = function(cls){
    $(cls).datepicker();
  }

}();
