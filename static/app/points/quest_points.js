var QuestPoints = function(settings) {

	var ex = {
		Map: '',
		Marker: '',
		mapContainerId: '#MapContainer',
		latitudeId: '#tbLatitude',
		longitudeId: '#tbLongitude',
		mapSize: 14
	},

    doInitPage = function() {
    	console.log('doInitPage--->');

    	$.extend(ex, settings);
		doInitMap();
	},

	doInitMap = function(){
		console.log('doInitMap--->');
		ex.Map = maputils.addMapToCanvas($(ex.mapContainerId), $(ex.latitudeId).val(), $(ex.longitudeId).val(), ex.mapSize);
		doInitRightClickMapHandler(ex.Map, ex.Marker, ex.latitudeId, ex.longitudeId);
	},

	doInitRightClickMapHandler = function(map, marker, latFieldId, lonFieldId){

		google.maps.event.addListener(map, "rightclick", function(event) {
				    var lat = event.latLng.lat(),
				        lng = event.latLng.lng(),
				        position = new google.maps.LatLng(lat, lng);
				    
				    this.micon = '/static/images/Gas-Soldier-icon_40x40.png';

				    if(marker){
				    	marker.setPosition(position);
				    }
				    else{
				    	marker = new google.maps.Marker({
					      position: position,
					      map: map,
					      title:"Hello World!",
					      icon: this.micon
					  	});	
				    }

				  	$(latFieldId).val(lat);
				    $(lonFieldId).val(lng);
				});
	}

	return {
		initPage: doInitPage
	};
}