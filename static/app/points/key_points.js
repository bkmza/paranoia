var KeyPoints = function(settings) {

	var ex = {
		Map: '',
		Marker: '',
		mapContainerId: '#MapContainer',
		latitudeId: '#tbLatitude',
		longitudeId: '#tbLongitude',
		mapSize: 14,
		keyFormId: '#KeyPointForm',
		keyListId: '#KeyListForm',
		addKeyBtnId: '#AddKeyPoint',
		saveBtnId: '#SaveBtn',
    keyItemCls: '.point-item'
	},

    doInitPage = function() {
    	console.log('doInitPage--->');

    	$.extend(ex, settings);
		  doInitMap();
		  doBindAddHandler();
		  doUpdateListForm();
	},

	doBindAddHandler = function(){
    $(ex.addKeyBtnId).on('click', function(){
        var token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
        utils.doAjaxForm(ex.urlForm, {}, doUpdateFormCallback);
    });
	},

	doBindSavePointHandler = function(){
    $(ex.saveBtnId).on('click', function(){
      doSaveDetailForm();
    });
	},

	doUpdateListForm = function(){
      utils.doAjaxForm(ex.urlListForm, {}, doRenderListForm);
	},

	doRenderListForm = function(data){
    $(ex.keyListId).replaceWith(data);

    doBindPointItemClick();
	},

	doBindPointItemClick = function(){
    $(ex.keyItemCls).on('click', function(){
      $(ex.keyItemCls).removeClass('active');
      $(this).addClass('active');
    });
	},

	doSaveDetailForm = function(){
      var form = $(ex.keyFormId).serialize();
      form['csrfmiddlewaretoken'] = document.getElementsByName('csrfmiddlewaretoken')[0].value;
      utils.doAjax(ex.urlAddOrUpdate, form, doAddOrUpdateCallback);
	},

	doUpdateFormCallback = function(data){
    $(ex.keyFormId).replaceWith(data);
    doInitSelects();
    doBindSavePointHandler();
    doInitDatepickers();
	},

	doAddOrUpdateCallback = function(data){
	  console.log(data);
	},

	doInitDatepickers = function(){
    utils.initDatepickers('.date-picker');
	},

	doInitSelects = function(){
    utils.initSelect2('.select2', "Select a Fraction");
	},

	doInitMap = function(){
		console.log('doInitMap--->');
		ex.Map = maputils.addMapToCanvas($(ex.mapContainerId), $(ex.latitudeId).val(), $(ex.longitudeId).val(), ex.mapSize);
		doInitRightClickMapHandler(ex.Map, ex.Marker, ex.latitudeId, ex.longitudeId);
	},

	doInitRightClickMapHandler = function(map, marker, latFieldId, lonFieldId){

		google.maps.event.addListener(map, "rightclick", function(event) {
				    var lat = event.latLng.lat(),
				        lng = event.latLng.lng(),
				        position = new google.maps.LatLng(lat, lng);
				    
				    this.micon = '/static/images/Gas-Soldier-icon_40x40.png';

				    if(marker){
				    	marker.setPosition(position);
				    }
				    else{
				    	marker = new google.maps.Marker({
					      position: position,
					      map: map,
					      title:"Hello World!",
					      icon: this.micon
					  	});	
				    }

				  	$(latFieldId).val(lat);
				    $(lonFieldId).val(lng);
				});
	}

	return {
		initPage: doInitPage
	};
}