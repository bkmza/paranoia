var FractionList = function (settings) {
    var ex = {
          tableId: '#fractionsListTable'
        },
        doInitPage = function() {
          $.extend(ex, settings);

          doBindRemoveBtn();
        },

        doBindRemoveBtn = function(){
          var token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
          $('.delete-item').on('click', function(){
            var id = $(this).closest('tr').attr('data-id');

            if(confirm('Are you sure that you want to delete this item?')){
              utils.doAjax(ex.urlDelete, {csrfmiddlewaretoken: token, id: id}, doCallbackRemove);
            }
          })
        },
        doCallbackRemove = function(data){
          if(data && data.is_success){
            location.reload();
          }
        };
    return {
        initPage: doInitPage
    }
}