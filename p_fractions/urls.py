from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from p_fractions import views


urlpatterns = patterns('',

    url(r'^list', views.fractions_list, name='fractions_list'),
    url(r'^add-update/((?P<id>\d+))?$', views.fractions_details, name='fractions_details'),
    url(r'^delete$', views.fraction_delete, name='fractions_delete'),
)