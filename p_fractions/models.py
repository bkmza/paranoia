from django.db import models
from p_alliances.models import Alliance


class Fraction(models.Model):
    name = models.CharField(max_length=200)
    game_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    alliance = models.ForeignKey(Alliance, unique=True)

    def __unicode__(self):
        return self.name
