from django import forms
from p_fractions.models import Fraction


class FractionForm(forms.ModelForm):

  class Meta:
    model = Fraction