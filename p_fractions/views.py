import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.core.urlresolvers import reverse

from p_fractions.forms import FractionForm
from p_fractions.models import Fraction


@login_required
@require_http_methods(["GET"])
def fractions_list(request):
    items = Fraction.objects.all()
    context = {'items': items}
    return render(request, 'f_list.html', context)


@login_required
@require_http_methods(["GET", "POST"])
def fractions_details(request, id=None):
    update_mode = False or (id is not None)
    context = {}
    if request.method == 'POST':
        new_fraction = None
        if update_mode:
            fraction = Fraction.objects.get(pk=id)
            fraction_form = FractionForm(request.POST, instance=fraction)
            if fraction_form.is_valid():
                new_fraction = fraction_form.save(commit=False)
                new_fraction.save()
        else:
            " handle creation of new objects "
            form = FractionForm(request.POST)
            if form.is_valid():
                new_fraction = form.save()
            else:
                return render(request, 'f_details.html', {'formset': [form]})

        if new_fraction is not None:
            return HttpResponseRedirect(reverse('p_fractions:fractions_list'))

    # GET
    else:
        if id is not None:
            " request for an edit form "
            fraction = Fraction.objects.get(id=id)
            fraction_form = FractionForm(instance=fraction)
            context['fraction_id'] = fraction.id
            context['formset'] = [fraction_form]
        else:
            " request for create form. Return empty "
            fraction_form = FractionForm()
            context['formset'] = [fraction_form]

    return render(request, 'f_details.html', context)


@login_required
@require_http_methods(["POST"])
def fraction_delete(request):
    Fraction.objects.filter(id=request.POST['id']).delete()
    return HttpResponse(json.dumps({'is_success':True }), content_type="application/json")