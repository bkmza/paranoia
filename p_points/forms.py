from django import forms
from p_points.models import KeyPoint, QuestPoint


class KeyPointForm(forms.ModelForm):
    class Meta:
        model = KeyPoint


class QuestPointForm(forms.ModelForm):
    class Meta:
        model = QuestPoint