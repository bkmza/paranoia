from django.utils import timezone
from django.db import models

from p_alliances.models import Alliance
from p_fractions.models import Fraction
from paranoia.models import UserProfile


class Point(models.Model):
    title = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now=True)
    date_updated = models.DateTimeField(auto_now=True)
    lat = models.FloatField()
    lon = models.FloatField()

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name


class KeyPoint(Point):
    fraction = models.ForeignKey(Fraction, related_name='keypoint_fraction')
    is_secret = models.BooleanField(default=False)
    income_personal = models.DecimalField(max_digits=5, decimal_places=2)
    income_fraction = models.DecimalField(max_digits=5, decimal_places=2)
    ctrl_fraction = models.ForeignKey(Fraction, related_name='keypoint_ctrl_fraction')
    owner = models.ForeignKey(UserProfile)
    distance_capture = models.FloatField()
    distance_open = models.FloatField()
    is_refresh = models.BooleanField(default=False)
    date_refresh = models.DateTimeField()


class QuestPoint(Point):
    walkthrough_limit = models.IntegerField(default=1)
    walkthrough_rest = models.IntegerField(default=1)
    loyalty = models.IntegerField()
    fraction = models.ForeignKey(Fraction)
    alliance = models.ForeignKey(Alliance)
    distance_take = models.FloatField()
    predecessor = models.ForeignKey('self')
    date_activated = models.DateTimeField()
    date_expiration = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    description_short = models.TextField()
    description_taking = models.TextField()


