from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import timezone
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_http_methods
from django.views.generic import CreateView, DetailView
from p_points.forms import KeyPointForm
from p_points.models import KeyPoint
import json


@login_required
@require_http_methods(["GET"])
def key_points(request):
    context = {}
    if request.is_ajax():
        key_point_form = KeyPointForm()
        context['formset'] = [key_point_form]
        template = 'key_form_partial.html'
    else:
        template = 'key_points.html'
    return render(request, template, context)


def get_key_points_form(request):
    context = {}
    key_point_form = KeyPointForm()
    context['formset'] = [key_point_form]
    return render_to_response('key_form_partial.html', context, context_instance=RequestContext(request))



def get_key_points_list_form(request):
    context = {'items': KeyPoint.objects.all()}
    return render_to_response('key_marker_list_partial.html', context, context_instance=RequestContext(request))


@require_http_methods(["POST"])
def add_or_update_point(request):
    context = {'is_success': True, 'message': 'Point has been added.'}
    form = KeyPointForm(request.POST)
    if form.is_valid():
        new_point = form.save()
        return HttpResponse(json.dumps(context), content_type="application/json")
    else:
        context['message'] = 'Point hasn\'t been added. Form is invalid.'
        context['errors'] = form.errors
        return HttpResponse(json.dumps(context), content_type="application/json")



@login_required
@require_http_methods(["GET"])
def quest_points(request):
    items = {}
    context = {'items': items}
    return render(request, 'quest_points.html', context)