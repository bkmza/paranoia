from django.conf.urls import patterns, url
from p_points import views


urlpatterns = patterns('',


    url(r'keys/get-form/$', views.get_key_points_form, name='get_key_points_form'),
    url(r'keys/get-list-form/$', views.get_key_points_list_form, name='get_key_points_list_form'),

    url(r'keys/add-or-update/$', views.add_or_update_point, name='add_or_update_point'),
    url(r'^keys/', views.key_points, name='key_points'),

    url(r'^quests', views.quest_points, name='quest_points')
)