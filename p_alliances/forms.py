from django import forms
from p_alliances.models import Alliance


class AllianceForm(forms.ModelForm):

  class Meta:
    model = Alliance