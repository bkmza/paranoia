from django.conf.urls import patterns, include, url
from p_alliances import views


urlpatterns = patterns('',

    url(r'^list', views.alliances_list, name='alliances_list'),
    url(r'^add-update/((?P<id>\d+))?$', views.addUpdateAlliance, name='addUpdateAlliance'),
    url(r'^delete$', views.alliance_delete, name='alliance_delete'),
)