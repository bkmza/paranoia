from django.db import models


class Alliance(models.Model):
    name = models.CharField(max_length=200)
    game_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name