from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.core.urlresolvers import reverse
import json
from django.views.decorators.http import require_http_methods
from p_alliances.forms import AllianceForm
from p_alliances.models import Alliance


@login_required
@require_http_methods(["GET"])
def alliances_list(request):
  items = Alliance.objects.all()
  context = { 'items':items }
  return render(request, 'list.html', context)


@login_required
@require_http_methods(["GET", "POST"])
def addUpdateAlliance(request, id=None):

  update_mode = False or (id is not None)
  context = {}
  if request.method == 'POST':
    new_alliance = None
    if update_mode:
      alliance = Alliance.objects.get(pk=id)
      aForm = AllianceForm(request.POST, instance=alliance)
      if aForm.is_valid():
        new_alliance = aForm.save(commit=False)
        new_alliance.save()
    else:
      " handle creation of new objects "
      form = AllianceForm({'name': request.POST['name'],
                        'game_name': request.POST['game_name'],
                        'color':request.POST['color']})
      if form.is_valid():
        new_alliance = form.save()
      else:
        return render(request, 'addUpdate.html', {'formset':[form]})


    if new_alliance is not None:
      return HttpResponseRedirect(reverse('p_alliances:alliances_list'))

  # GET
  else:
    if id is not None:
      " request for an edit form "
      alliance = Alliance.objects.get(id=id)
      aForm = AllianceForm(instance=alliance)
      context['alliance_id'] = alliance.id
      context['formset'] = [aForm]
    else:
      " request for create form. Return empty "
      allianceForm = AllianceForm()
      context['formset'] = [allianceForm]

  return render(request, 'addUpdate.html', context)


@login_required
@require_http_methods(["POST"])
def alliance_delete(request):
    Alliance.objects.filter(id=request.POST['id']).delete()
    return HttpResponse(json.dumps({'is_success':True }), content_type="application/json")